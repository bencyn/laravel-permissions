### Installation
* `git clone  https://gitlab.com/bencyn/laravel-permissions.git`

* `cd acl`

* `composer install`

* save the .env.example to .env

* update the .env file with your db credentials

* `php artisan key:generate`
 
* run  `php artisan db:seed` 

* to login as an admin user

* username : admin@example.com
* password : secret
 
* to login as a writer

* username : test@example.com
* password : secret


